package project.rawg.utils

object Arguments {
    const val GAME_DATA = "GAME_DATA"
    const val SCREENSHOT_DATA = "SCREENSHOT_DATA"
}
